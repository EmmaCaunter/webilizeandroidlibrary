# README #

This README documents downloading, setting up and using this library module.

### What is this repository for? ###

A collection of commonly written Android classes:

Networking

Http

Email

Kml

SQLite Database

### How do I get set up? ###

Pull library with Git.

Open desired project in Android Studio. Click File -> New -> Import Module.

Navigate to webilizeandroidlibrary and select. Library should open in the same window as project.

Right click on project and select "Open Module Settings".

Select "Dependencies".

Click the green plus button at the top right hand side to add new dependency. Select "Module Dependency".

Select "webilizeandroidlibrary"

### Usage ###

**-- Network Helper --**

    NetworkHelper.isInternetAvailable();

Returns true if the device is connected to the internet. Works for WiFi and Cellular data.



**-- KML Parser --**

    KmlParser parser = new KmlParser();

    parser.parseKML("kml_file_name.kml", context)

    if (parser.containsPoint(latLng) {

        // CODE

    }

    parser.containsPoint(latLng)
 
returns true if the specified latitude/longitude is within the area
bounded by the kml file.

**-- Email Helper --**

    EmailHelper.sendEmail(addresses, subject, body, displayText, returnActivityType, activity)

addresses: list of strings representing email addresses. The email will be sent to all addresses.

subject: subject of email.

body: text body of email.

displayText: String to be displayed by application as device is sending email.

returnActivityType: integer id to be returned to the OnActivityResult method.

activity: activity that will call the intent and will have its OnActivityResult method called.



    EmailHelper.sendEmail(addresses, subject, body, displayText, attachment, returnActivityType, activity)


attachment: Uri containing data (like an image) to be attached to the email.



**-- Database Helper --**


Store original sqlite database file in the assets folder.

    DatabaseHelper helper = new DatabaseHelper(context, path, name);

path: desired path to database when stored on device. Suggested use is "/data/data/com.yourpackage.name/database/"

name: name of original database file.

    try {
        databaseHelper.createDatabase();
    } catch (IOException ioe) {
        throw new Error("Unable to create database");
    }
    try {
        databaseHelper.openDatabase();
    } catch (SQLException sqle) {
        throw sqle;
    }

    synchronized (this) {

        SQLiteCursor cursor = (SQLiteCursor) databaseHelper.getReadableDatabase().query(false, // Distinct

                    "table_name", new String[] { "column1", "column2", "column3", ...}, selection_string, selection_args, group_by, having, order_by, limit);

        for (int i = 0; i < cursor.getCount(); i++) {
            if (cursor.moveToPosition(i)) {
                // CODE
            }
        }    
    } 




**-- HTTPS Helper --**

    HttpsHelper httpsHelper = new HttpsHelper(apiString);

apiString: base string for api


    sendJsonBodyPost(JSONObject jsonObject, String urlExtension, OnPostCompletedListener taskListener, int id)

jsonObject: json body for post

urlExtension: extension for base url (example: apiString = "www.mycompany.com" urlExtension = "/api/changepassword"

listener: callback for on complete

id: integer to identify a particular post


    sendJsonBodyPostWithToken(JSONObject jsonObject, String token, String urlExtension, OnPostCompletedListener taskListener, int id)

token: string for authentication in content header


    getJsonWithToken(String token, String urlExtension, OnGetJsonCompletedListener taskListener, int id)

as above


    getJson(String urlExtension, OnGetJsonCompletedListener taskListener, int id)

as above


    getJsonWithTokenAndParameter(String token, String parameterName, String parameter, String urlExtension, OnGetJsonCompletedListener taskListener, int id)
as above except:

parameterName: string for specification parameter name

parameter: string for specification parameter

example: getJsonWithTokenAndParameter(token, "userName", "adam", "getAge", listener, 0) will create url "www.mycompany.com/getAge?userName=adam"


    getJsonWithTokenAndParameters(String token, String[] parameterNames, String[] parameters, String urlExtension, OnGetJsonCompletedListener taskListener, int id)

as above except with multiple parameters. The lists parameterNames and parameters MUST have the same length.

    postJsonWithToken(String token, JSONObject jsonObject, String urlExtension, OnPostCompletedListener taskListener, int id)
as above

    sendUrlEncodedPost(String[] keys, String[] values, String urlExtension, OnPostCompletedListener taskListener, int id)
keys and values MUST have the same length.

creates post with content type application/x-www-form-urlencoded. Keys and values are combined in the following manner:

"key1=value1&key2=value2&etc.."

**-Callbacks:**

implement either OnPostCompletedListener or OnGetJsonCompletedListener for alerts when async network task has completed and to receive success/failure codes and result data.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Emma
* Repo owner or admin
* Other community or team contact