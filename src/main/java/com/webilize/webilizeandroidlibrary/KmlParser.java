package com.webilize.webilizeandroidlibrary;

import android.content.Context;
import android.content.res.AssetManager;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by webdev11 on 2017-05-26.
 *
 * Usage:
 * KmlParser parser = new KmlParser();
 * parser.parseKML("kml_file_name.kml", context)
 * if (parser.containsPoint(latLng) {
 *     // CODE
 * }
 *
 */

public class KmlParser {

    private String polygon = "Polygon";
    private String coordinates = "coordinates";
    private XmlPullParser myparser;

    private ArrayList<ArrayList<LatLng>> placemarks;

    private XmlPullParserFactory xmlFactoryObject;

    public KmlParser() {
        try {
            this.placemarks = new ArrayList<>();
            xmlFactoryObject = XmlPullParserFactory.newInstance();
            myparser = xmlFactoryObject.newPullParser();
            myparser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);

        } catch (XmlPullParserException e) {

        }
    }

    private void parseAndStoreKML() {
        int event;
        String text=null;
        try {
            event = myparser.getEventType();
            while (event != XmlPullParser.END_DOCUMENT) {
                String name = myparser.getName();

                switch (event){
                    case XmlPullParser.START_TAG:
                        if(name.equals(polygon)) {
                            ArrayList<LatLng> newPlacemark = new ArrayList<LatLng>();
                            this.placemarks.add(newPlacemark);
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = myparser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if(name.equals(coordinates)){
                            processCoordinates(text);
                        }

                        else{
                        }
                        break;
                }
                event = myparser.next();
            }

        } catch (XmlPullParserException e) {

        } catch (IOException e2) {

        }
    }

    public boolean containsPoint(LatLng point) {
        for (ArrayList<LatLng> polygon : this.placemarks) {
            if (isPointInPolygon(point, polygon)) {
                return true;
            }
        }
        return false;
    }

    private boolean isPointInPolygon(LatLng tap, ArrayList<LatLng> vertices) {
        int intersectCount = 0;
        for (int j = 0; j < vertices.size() - 1; j++) {
            if (rayCastIntersect(tap, vertices.get(j), vertices.get(j + 1))) {
                intersectCount++;
            }
        }

        return ((intersectCount % 2) == 1); // odd = inside, even = outside;
    }

    private boolean rayCastIntersect(LatLng tap, LatLng vertA, LatLng vertB) {

        double aY = vertA.latitude;
        double bY = vertB.latitude;
        double aX = vertA.longitude;
        double bX = vertB.longitude;
        double pY = tap.latitude;
        double pX = tap.longitude;

        if ((aY > pY && bY > pY) || (aY < pY && bY < pY)
                || (aX < pX && bX < pX)) {
            return false; // a and b can't both be above or below pt.y, and a or
            // b must be east of pt.x
        }

        double m = (aY - bY) / (aX - bX); // Rise over run
        double bee = (-aX) * m + aY; // y = mx + b
        double x = (pY - bee) / m; // algebra is neat!

        return x > pX;
    }

    private void processCoordinates(String values) {
        String[] triples = values.split(" ");
        int index = this.placemarks.size() - 1;
        ArrayList<LatLng> coordList = this.placemarks.get(index);
        for (String triple : triples ) {
            String[] coords = triple.split(",");
            if (coords.length > 1) {
                LatLng latLng = new LatLng(Double.parseDouble(coords[1]), Double.parseDouble(coords[0]));
                coordList.add(latLng);
            }
        }
        coordList.add(coordList.get(0));
    }

    public void parseKML(String filepath, Context context) {
        try {
            AssetManager assetManager = context.getAssets();
            InputStream fis = assetManager.open(filepath);
            myparser.setInput(fis, null);
            parseAndStoreKML();
            fis.close();
        } catch (IOException e2) {

        } catch (XmlPullParserException e) {

        }

    }
}
