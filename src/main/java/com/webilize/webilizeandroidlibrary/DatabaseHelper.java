package com.webilize.webilizeandroidlibrary;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by webdev11 on 2017-05-26.
 *
 * Usage:
 *
 * Store database file in assets folder.
 *
 * DatabaseHelper helper = new DatabaseHelper(context, path, name);
 * Where name = name of database file
 * and path = desired path to database on device. Suggested use is "/data/data/com.package.name/databases/"
 *
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private String DB_PATH;
    private String DB_NAME;

    private Context context;
    private SQLiteDatabase myDataBase;


    public DatabaseHelper(Context context, String path, String name) {
        super(context, name, null, 1);
        this.DB_NAME = name;
        this.DB_PATH = path;
        this.context = context;
    }

    public DatabaseHelper(Context context, String path, String name, int version, SQLiteDatabase.CursorFactory cursorFactory) {
        super(context, name, cursorFactory, version);
        this.DB_NAME = name;
        this.DB_PATH = path;
        this.context = context;
    }

    public void openDatabase() throws SQLException {

        //Open the database
        String myPath = DB_PATH + DB_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY | SQLiteDatabase.NO_LOCALIZED_COLLATORS);

    }

    public void createDatabase() throws IOException {

        boolean dbExist = checkDatabase();
        if(dbExist){
            //do nothing - database already exist
        }else{

            //By calling this method and empty database will be created into the default system path
            //of your application so we are gonna be able to overwrite that database with our database.
            this.getReadableDatabase();
            try {
                copyDatabase();
            } catch (IOException e) {
                throw new Error("Error copying database");
            }
        }
    }

    private boolean checkDatabase(){

        SQLiteDatabase checkDB = null;
        try{
            String myPath = DB_PATH + DB_NAME;
            File databasePath = context.getDatabasePath(myPath);
            return databasePath.exists();
        }catch(SQLiteException e){
            //database does't exist yet.
        }
        if(checkDB != null){
            checkDB.close();
        }
        return checkDB != null ? true : false;
    }

    private void copyDatabase() throws IOException {

        //Open local db as the input stream
        InputStream myInput = context.getAssets().open(DB_NAME);

        // Path to the just created empty db
        String outFileName = DB_PATH + DB_NAME;

        //Open the empty db as the output stream
        OutputStream myOutput = new FileOutputStream(outFileName);

        //transfer bytes from the inputfile to the outputfile
        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer))>0){
            myOutput.write(buffer, 0, length);
        }

        //Close the streams
        myOutput.flush();
        myOutput.close();
        myInput.close();

    }

    @Override
    public synchronized void close() {
        if(myDataBase != null)
            myDataBase.close();
        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private void deleteDatabase() {
        File file = new File(DB_PATH + DB_NAME);
        file.delete();
    }

}
