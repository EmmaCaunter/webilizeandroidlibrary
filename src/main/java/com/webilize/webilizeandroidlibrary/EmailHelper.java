package com.webilize.webilizeandroidlibrary;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/**
 * Created by webdev11 on 2017-05-26.
 */

public class EmailHelper {

    public static void sendEmail(String[] toAddresses, String subject, String body, String displayText, int returnActivityType, Activity activity) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , toAddresses);
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT   , body);

        try {
            activity.startActivityForResult(Intent.createChooser(i, displayText), returnActivityType);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public static void sendEmailWithAttachment(String[] toAddresses, String subject, String body, String displayText, Uri attachment, int returnActivityType, Activity activity) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , toAddresses);
        i.putExtra(Intent.EXTRA_SUBJECT, subject);
        i.putExtra(Intent.EXTRA_TEXT   , body);
        if (attachment != null)
        {
            i.putExtra(Intent.EXTRA_STREAM, attachment);
        }

        try {
            activity.startActivityForResult(Intent.createChooser(i, displayText), returnActivityType);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(activity, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }


    }
}
