package com.webilize.webilizeandroidlibrary.http;

/**
 * Created by webdev11 on 2017-05-29.
 */

public class HttpResult {

    public enum HttpResultStatus {

        SUCCESS,
        FAILED
    }

    public String message;
    public HttpResultStatus status;
    public int httpErrorCode;
    public int id;

    public HttpResult (String message, HttpResultStatus status, int httpErrorCode, int id) {
        this.status = status;
        this.message = message;
        this.httpErrorCode = httpErrorCode;
        this.id = id;
    }

}
