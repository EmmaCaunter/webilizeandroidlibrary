package com.webilize.webilizeandroidlibrary.http;

/**
 * Created by webdev11 on 2017-05-29.
 */

public class Post {

    public String url;
    public String urlParameters;
    public String token;

    public Post(String url, String parameters) {
        this.url = url;
        this.urlParameters = parameters;
    }

    public Post(String url, String parameters, String token) {
        this.url = url;
        this.urlParameters = parameters;
        this.token = token;
    }
}
