package com.webilize.webilizeandroidlibrary.http;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by webdev11 on 2017-05-29.
 */

public class GetJsonTask extends AsyncTask<Get, Void, HttpJsonResult> {

    private HttpsHelper.OnGetJsonCompletedListener listener;
    private int id;

    public GetJsonTask(HttpsHelper.OnGetJsonCompletedListener listener, int id) {
        this.listener = listener;
        this.id = id;
    }

    protected HttpJsonResult doInBackground(Get... gets) {
        Get get = gets[0];
        try {
            URL url = new URL(get.url);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            if (get.token != null) {
                connection.setRequestProperty("Authorization", get.token);
            }

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                connection.disconnect();
                JSONObject resultObject = new JSONObject(response.toString());
                return new HttpJsonResult(resultObject, "Success", HttpResult.HttpResultStatus.SUCCESS, 0, id);
            } else if (responseCode == -1) {
                return new HttpJsonResult(null, "Invalid Http", HttpResult.HttpResultStatus.FAILED, responseCode, id);
            } else {
                return new HttpJsonResult(null, "Http error", HttpResult.HttpResultStatus.FAILED, responseCode, id);
            }

        } catch  (MalformedURLException e) {
            return new HttpJsonResult(null, "Malformed URL", HttpResult.HttpResultStatus.FAILED, -1, id);
        } catch (IOException e2) {
            e2.printStackTrace();
            return new HttpJsonResult(null, "Unknown IO exception", HttpResult.HttpResultStatus.FAILED, -1, id);
        } catch (JSONException e3) {
            return new HttpJsonResult(null, "Bad json response", HttpResult.HttpResultStatus.FAILED, -1, id);
        }
    }

    protected void onPostExecute(HttpJsonResult result) {
        listener.onGetCompleted(result);
    }
}
