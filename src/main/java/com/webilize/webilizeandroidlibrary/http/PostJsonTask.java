package com.webilize.webilizeandroidlibrary.http;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by webdev11 on 2017-05-29.
 */

public class PostJsonTask extends AsyncTask<Post, Void, HttpResult> {

    private HttpsHelper.OnPostCompletedListener listener;
    private int id;

    public PostJsonTask(HttpsHelper.OnPostCompletedListener listener, int id) {
        this.listener = listener;
        this.id = id;
    }

    protected HttpResult doInBackground(Post... posts) {
        Post post = posts[0];
        try {
            URL url = new URL(post.url);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            if (post.token != null) {
                connection.setRequestProperty("Authorization", post.token);
            }
            connection.setDoInput(true);
            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
            wr.write(post.urlParameters);
            wr.flush();
            wr.close();

            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                connection.disconnect();
                return new HttpResult("Success: " + response.toString(), HttpResult.HttpResultStatus.SUCCESS, 0, id);
            } else if (responseCode == -1) {
                return new HttpResult("Invalid Http", HttpResult.HttpResultStatus.FAILED, responseCode, id);
            } else {
                return new HttpResult("Http error", HttpResult.HttpResultStatus.FAILED, responseCode, id);
            }

        } catch  (MalformedURLException e) {
            return new HttpResult("Malformed URL", HttpResult.HttpResultStatus.FAILED, -1, id);
        } catch (IOException e2) {
            return new HttpResult("Unknown IO exception", HttpResult.HttpResultStatus.FAILED, -1, id);
        }
    }

    protected void onPostExecute(HttpResult result) {
        listener.onTaskCompleted(result);
    }
}
