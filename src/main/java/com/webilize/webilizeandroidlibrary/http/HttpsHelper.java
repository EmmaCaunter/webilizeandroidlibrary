package com.webilize.webilizeandroidlibrary.http;

import org.json.JSONObject;

/**
 * Created by webdev11 on 2017-05-26.
 */

public class HttpsHelper {

    public String API_CONNECTION_STRING;

    public HttpsHelper(String apiString) {
        this.API_CONNECTION_STRING = apiString;
    }

    public void sendJsonBodyPost(JSONObject jsonObject, String urlExtension, OnPostCompletedListener taskListener, int id) {
        String urlString = API_CONNECTION_STRING + urlExtension;
        new PostJsonTask(taskListener, id).execute(new Post(urlString, jsonObject.toString()));
    }

    public void sendJsonBodyPostWithToken(JSONObject jsonObject, String token, String urlExtension, OnPostCompletedListener taskListener, int id) {
        String urlString = API_CONNECTION_STRING + urlExtension;
        new PostJsonTask(taskListener, id).execute(new Post(urlString, jsonObject.toString(), token));
    }

    public void getJsonWithToken(String token, String urlExtension, OnGetJsonCompletedListener taskListener, int id) {
        String urlString = API_CONNECTION_STRING + urlExtension;
        new GetJsonTask(taskListener, id).execute(new Get(urlString, token));
    }

    public void getJson(String urlExtension, OnGetJsonCompletedListener taskListener, int id) {
        String urlString = API_CONNECTION_STRING + urlExtension;
        new GetJsonTask(taskListener, id).execute(new Get(urlString));
    }

    public void getJsonWithTokenAndParameter(String token, String parameterName, String parameter, String urlExtension, OnGetJsonCompletedListener taskListener, int id) {
        String urlString = API_CONNECTION_STRING + urlExtension + String.format("?%s=%s", parameterName, parameter);
        new GetJsonTask(taskListener, id).execute(new Get(urlString, token));
    }

    public void getJsonWithParameter(String parameterName, String parameter, String urlExtension, OnGetJsonCompletedListener taskListener, int id) {
        String urlString = API_CONNECTION_STRING + urlExtension + String.format("?%s=%s", parameterName, parameter);
        new GetJsonTask(taskListener, id).execute(new Get(urlString));
    }

    public boolean getJsonWithTokenAndParameters(String token, String[] parameterNames, String[] parameters, String urlExtension, OnGetJsonCompletedListener taskListener, int id) {
        String urlString = API_CONNECTION_STRING + urlExtension;
        if (parameterNames.length != parameters.length) {
            return false;
        }
        for (int i = 0; i < parameterNames.length; i ++) {
            urlString += String.format("?%s=%s", parameterNames[i], parameters[i]);
            if (i < parameterNames.length - 1) {
                urlString += "&";
            }
        }
        new GetJsonTask(taskListener, id).execute(new Get(urlString, token));
        return true;
    }

    public boolean getJsonWithParameters(String[] parameterNames, String[] parameters, String urlExtension, OnGetJsonCompletedListener taskListener, int id) {
        String urlString = API_CONNECTION_STRING + urlExtension;
        if (parameterNames.length != parameters.length) {
            return false;
        }
        for (int i = 0; i < parameterNames.length; i ++) {
            urlString += String.format("?%s=%s", parameterNames[i], parameters[i]);
            if (i < parameterNames.length - 1) {
                urlString += "&";
            }
        }
        new GetJsonTask(taskListener, id).execute(new Get(urlString));
        return true;
    }

    public void postJsonWithToken(String token, JSONObject jsonObject, String urlExtension, OnPostCompletedListener taskListener, int id) {
        String urlString = API_CONNECTION_STRING + urlExtension;
        new PostJsonTask(taskListener, id).execute(new Post(urlString, jsonObject.toString(), token));
    }

    public boolean sendUrlEncodedPost(String[] keys, String[] values, String urlExtension, OnPostCompletedListener taskListener, int id) {

        if (keys.length != values.length) {
            return false;
        }

        String urlParameters = "";
        for (int i = 0; i < keys.length; i ++) {
            urlParameters += keys[i] + "=" + values[i];
            if (i != keys.length - 1) {
                urlParameters += "&";
            }
        }

        String urlString = API_CONNECTION_STRING + urlExtension;
        new PostUrlEncodedTask(taskListener, id).execute(new Post(urlString, urlParameters));
        return true;
    }




    public interface OnPostCompletedListener {
        void onTaskCompleted(HttpResult result);
    }

    public interface OnGetJsonCompletedListener {
        void onGetCompleted(HttpJsonResult jsonResult);
    }

}
