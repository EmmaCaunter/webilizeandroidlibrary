package com.webilize.webilizeandroidlibrary.http;

import org.json.JSONObject;

/**
 * Created by webdev11 on 2017-05-29.
 */

public class HttpJsonResult extends HttpResult {

    public JSONObject jsonObject;

    public HttpJsonResult (JSONObject object, String message, HttpResult.HttpResultStatus status, int httpErrorCode, int id) {
        super(message, status, httpErrorCode, id);
        this.jsonObject = object;
    }

}
