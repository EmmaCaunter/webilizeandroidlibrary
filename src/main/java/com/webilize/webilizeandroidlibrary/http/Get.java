package com.webilize.webilizeandroidlibrary.http;

/**
 * Created by webdev11 on 2017-05-29.
 */

public class Get {

    public String url;
    public String urlParameters;
    public String token;

    public Get(String url) {
        this.url = url;
    }

    public Get(String url, String token) {
        this.url = url;
        this.token = token;
    }
}
